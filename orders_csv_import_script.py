import csv

orders = {}
output = {}
top_customers = {}
unique_barcodes = set([])

with open('orders.csv') as csvfile:
    orders_iterator = csv.DictReader(csvfile)
    for row in orders_iterator:
        entry_orders = {row['order_id'] : row['customer_id']}
        orders.update(entry_orders)
        entry_output = { (row['customer_id'], row['order_id']) : [] }
        output.update(entry_output)

with open('barcodes.csv') as csvfile:
    barcodes_iterator = csv.DictReader(csvfile)
    for row in barcodes_iterator:
        if row['barcode'] not in unique_barcodes:
            customer_id = orders.get(row['order_id'])
            current_key = (customer_id, row['order_id'])
            barcodes_list = output.get(current_key, [])
            barcodes_list.append(row['barcode'])
            entry_output = {current_key : barcodes_list}
            output.update(entry_output)
            unique_barcodes.add(row['barcode'])

with open('output.csv', 'w', newline='') as csvfile:
    fieldnames = ['customer_id', 'order_id', 'barcodes_list']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    for k, v in output.items():
        if k[0] and k[1] and v:
            writer.writerow({'customer_id': int(k[0]), 'order_id': int(k[1]), 'barcodes_list': v})

for k,v in output.items():
    count = top_customers.get(k[0], 0)
    count += len(v)
    if k[0]:
        top_customers.update({k[0] : count})

print('Top customers in descending order [customer_id , #tickets]:')
sorted_top_customers = sorted(top_customers, key=top_customers.get, reverse=True)
for k in sorted_top_customers[:5]:
    print(k , ',' , top_customers.get(k))

unused_barcodes = output.get((None,''))
print('\n%d unused barcodes!' % (len(unused_barcodes)))
