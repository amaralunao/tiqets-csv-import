import os

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count

from csvapi import models

class Command(BaseCommand):
    help = "Shows how many barcodes are left"


    def handle(self, *args, **options):
        count = 0

        barcodes = models.Product.objects.filter(order__isnull=True)
        count = barcodes.count()

        self.stdout.write(self.style.SUCCESS('There are %s barcodes left' %(count)))
