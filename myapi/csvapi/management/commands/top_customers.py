from operator import itemgetter

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count

from csvapi import models
from csvapi.constants import top_customers_count


class Command(BaseCommand):
    help = "Shows top %s customers based on number of tickets bought" % top_customers_count


    def handle(self, *args, **options):
        output = []

        customers = models.Customer.objects.all().prefetch_related('orders')
        for customer in customers:
            tickets_count = customer.orders.aggregate(tickets_count=Count('order_items'))['tickets_count']
            customer_id = customer.customer_id
            output.append((customer_id, tickets_count))

        sorted_output = sorted(output, key=itemgetter(1), reverse=True)
        for row in sorted_output[:top_customers_count]:
            self.stdout.write(self.style.SUCCESS('%s, %s' %(row[0], row[1])))
