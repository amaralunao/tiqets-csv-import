import csv

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError

from csvapi import constants, models


class Command(BaseCommand):
    help = "Import barcodes and orders csv files from path"

    def add_arguments(self, parser):
        parser.add_argument('csv_type', type=str)
        parser.add_argument('csv_path', type=str)

    def handle(self, *args, **options):
        csv_type = options['csv_type']
        csv_path = options['csv_path']
        import_errors = []
        if csv_type == constants.barcodes:
            import_errors = import_barcodes(csv_path)
        elif csv_type == constants.orders:
            import_orders(csv_path)
        else:
            raise CommandError('Please provide valid csv type \
                                \nValid choices are: "%s" and "%s"' % (constants.orders,
                                                               constants.barcodes))

        if import_errors:
            for row in import_errors:
                self.stdout.write(self.style.NOTICE(row))

        self.stdout.write(self.style.SUCCESS(
            "Successfuly imported CSV file to database"))


def import_orders(csv_path):
    with open(csv_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            customer, customer_created = \
                    models.Customer.objects.get_or_create(customer_id=row['customer_id'])
            order, order_created = \
                    models.Order.objects.get_or_create(order_id=row['order_id'], \
                                defaults={'customer': customer})


def import_barcodes(csv_path):
    with open(csv_path) as csvfile:
        reader = csv.DictReader(csvfile)
        import_errors = []
        for row in reader:
            if row['order_id'] and not row['barcode']:
                import_errors.append('Order %s is without a barcode and was not \
                        updated\n' % row['order_id'])
                continue
            elif row['order_id'] and row['barcode']:
                product, product_created = models.Product.objects.get_or_create(
                        barcode=row['barcode'])
                order, order_created = \
                        models.Order.objects.get_or_create(order_id=row['order_id'])
                order_item, order_item_created = \
                        models.OrderItem.objects.get_or_create(order=order, product=product)
            elif row['barcode'] and not row['order_id']:
                barcode, created = models.Product.objects.get_or_create(barcode=row['barcode'])
        clean_up_orders_without_barcodes_from_database() #Based on assumption that no orders without barcodes should be allowed.
        return import_errors


def clean_up_orders_without_barcodes_from_database():
    queryset = models.Order.objects.filter(order_items__isnull=True)
    queryset.delete()
