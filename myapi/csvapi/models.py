from django.db import models


class Customer(models.Model):
    customer_id = models.PositiveIntegerField(primary_key=True)

    def __str__(self):
        return str(self.customer_id)


class Product(models.Model):
    barcode = models.BigIntegerField(primary_key=True)

    def __str__(self):
        return str(self.barcode)


class Order(models.Model):
    order_id = models.PositiveIntegerField(primary_key=True)
    customer = models.ForeignKey('Customer', related_name='orders')
    order_items = models.ManyToManyField(Product, through='OrderItem')


    def __str__(self):
        return str(self.order_id)


class OrderItem(models.Model):
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order)
