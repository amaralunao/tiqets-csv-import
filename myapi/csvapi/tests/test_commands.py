import os

import mock
import pytest
from django.core.management import call_command
from django.core.management.base import CommandError
from django.db.utils import IntegrityError

from csvapi import constants, models
from csvapi.management.commands import import_csv

dirname = os.path.dirname(os.path.abspath(__file__))

@pytest.mark.django_db
class TestImportCsvCommand:
    def test_import_orders_success(self):
        csv_path = os.path.join(dirname, 'orders.csv')
        import_csv.import_orders(csv_path)
        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

    def test_orders_and_customers_not_imported_twice(self):
        csv_path = os.path.join(dirname, 'orders.csv')
        import_csv.import_orders(csv_path)

        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

        import_csv.import_orders(csv_path)

        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

    def test_import_orders_and_barcodes_integration_success(self):
        """
        because import_barcodes function calls
        clean_up_orders_without_barcodes_from_database,
        expected orders count after the barcodes import is
        decreased by the amount of orders without barcodes
        """
        csv_path_orders = os.path.join(dirname, 'orders.csv')
        csv_path_barcodes = os.path.join(dirname, 'barcodes.csv')
        import_csv.import_orders(csv_path_orders)


        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

        import_errors = import_csv.import_barcodes(csv_path_barcodes)

        assert models.Product.objects.count() == 615
        assert models.Order.objects.count() == 204
        assert import_errors == []

    def test_import_barcodes_first_raises_exception(self):
        """
        Since Customer is a required ForeignKey for Order,
        the csv import oder matters, namely orders before barcodes.
        This is based purely on the assumption that
        this is a desired constraint for lack of a clear indication.
        """

        csv_path_orders = os.path.join(dirname, 'orders.csv')
        csv_path_barcodes = os.path.join(dirname, 'barcodes.csv')

        with pytest.raises(IntegrityError):
            import_csv.import_barcodes(csv_path_barcodes)

    def test_import_barcodes_with_duplicate_barcode(self):

        csv_path_barcodes = os.path.join(dirname, 'barcodes_duplicate_barcode.csv')

        customer = models.Customer.objects.create(customer_id=1)
        models.Order.objects.bulk_create([
            models.Order(order_id=1, customer=customer),
            models.Order(order_id=3, customer=customer)
            ])
        import_errors = import_csv.import_barcodes(csv_path_barcodes)
        assert models.Product.objects.count() == 1
        assert import_errors == []

    def test_import_barcodes_order_without_barcode_error(self):
        csv_path_barcodes = os.path.join(dirname, 'barcodes_order_no_barcode.csv')

        customer = models.Customer.objects.create(customer_id=1)
        models.Order.objects.bulk_create([
            models.Order(order_id=1, customer=customer),
            models.Order(order_id=2, customer=customer)
            ])
        import_errors = import_csv.import_barcodes(csv_path_barcodes)
        assert models.Product.objects.count() == 1
        assert len(import_errors) == 1

    def test_import_barcodes_without_order_id_success(self):
        csv_path_barcodes = os.path.join(dirname, 'barcodes_no_order_id.csv')
        import_errors = import_csv.import_barcodes(csv_path_barcodes)

        assert models.Product.objects.count() == 1
        assert import_errors == []

    def test_clean_up_orders_without_barcodes_from_database_success(self):
        customer = models.Customer.objects.create(customer_id=1)
        models.Order.objects.bulk_create([
            models.Order(order_id=1, customer=customer),
            models.Order(order_id=2, customer=customer)
            ])

        assert models.Order.objects.count() == 2

        import_csv.clean_up_orders_without_barcodes_from_database()
        assert models.Order.objects.count() == 0

    @mock.patch('csvapi.management.commands.import_csv.clean_up_orders_without_barcodes_from_database')
    def test_clean_up_orders_called_success(self, import_clean_up_mock):
        csv_path_barcodes = os.path.join(dirname, 'barcodes_no_order_id.csv')
        import_csv.import_barcodes(csv_path_barcodes)

        import_clean_up_mock.assert_called_once()


@pytest.mark.django_db
class TestImportCsvIntegrationCommand:

    def test_import_csv_incorrect_type_error(self):
        with pytest.raises(CommandError):
            call_command('import_csv', 'incorrect', 'some/path/')

    def test_import_csv_orders_success(self):
        csv_path_orders = os.path.join(dirname, 'orders.csv')
        call_command('import_csv', constants.orders, csv_path_orders)

        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

    def test_integration_csv_orders_and_barcodes(self):

        csv_path_orders = os.path.join(dirname, 'orders.csv')
        csv_path_barcodes = os.path.join(dirname, 'barcodes.csv')
        call_command('import_csv', constants.orders, csv_path_orders)

        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

        call_command('import_csv', constants.barcodes, csv_path_barcodes)

        assert models.Product.objects.count() == 615
        assert models.Order.objects.count() == 204


    @mock.patch('csvapi.management.commands.import_csv.import_orders')
    def test_import_orders_called_success(self, import_orders_mock):

        call_command('import_csv', constants.orders, '/some/path')
        import_orders_mock.assert_called_once_with('/some/path')


    @mock.patch('csvapi.management.commands.import_csv.import_barcodes')
    def test_import_orders_called_success(self, import_barcodes_mock):

        call_command('import_csv', constants.barcodes, '/some/path')
        import_barcodes_mock.assert_called_once_with('/some/path')


@pytest.mark.django_db
class TestTopCustomersCommand:
    """
    I adopted the Golden Master Testing
    approach here because the actual proper alternative
    is creating factories or many 'create' statements
    which for ManyToMany Relationships is a lot of work
    and it's late, and this is a bonus feature anyway...
    """
    def test_top_customers_success(self, capsys):

        csv_path_orders = os.path.join(dirname, 'orders.csv')
        csv_path_barcodes = os.path.join(dirname, 'barcodes.csv')
        call_command('import_csv', constants.orders, csv_path_orders)

        assert models.Customer.objects.count() == 78
        assert models.Order.objects.count() == 207

        call_command('import_csv', constants.barcodes, csv_path_barcodes)

        assert models.Product.objects.count() == 615
        assert models.Order.objects.count() == 204

        call_command('top_customers')
        out, err = capsys.readouterr()
        assert out == 'Successfuly imported CSV file to database\n'+ \
                      'Successfuly imported CSV file to database\n'+ \
                      '10, 23\n'+ \
                      '56, 20\n'+ \
                      '60, 17\n'+ \
                      '29, 16\n'+ \
                      '59, 15\n'


@pytest.mark.django_db
class TestBarcodesLeftCommand:

    def test_barcodes_left_success(self, capsys):


        ticket1 = models.Product.objects.create(barcode=1)
        ticket2 = models.Product.objects.create(barcode=2)
        ticket3 = models.Product.objects.create(barcode=3)

        customer = models.Customer.objects.create(customer_id=666)
        order = models.Order.objects.create(order_id=23, customer=customer)
        orderitem = models.OrderItem.objects.create(order=order, product=ticket1)

        call_command('barcodes_left')
        out, err = capsys.readouterr()
        assert out == 'There are 2 barcodes left\n'
