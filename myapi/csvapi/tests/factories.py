import pytest
from pytest_factoryboy import register
import factory

from csvapi.models import CSVUrl, Image
from csvapi.tests.helper import main_csv_url, main_image_url


class CSVUrlFactory(factory.DjangoModelFactory):
    csv_url = main_csv_url

    class Meta:
        model = CSVUrl


class ImageFactory(factory.DjangoModelFactory):

    url = factory.SubFactory(CSVUrlFactory)
    title = factory.Sequence(lambda n: 'Title%d' % n)
    description = factory.Sequence(lambda n: 'Description%d' % n)
    image = main_image_url

    class Meta:
        model = Image
