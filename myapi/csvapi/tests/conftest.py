import pytest

from csvapi.management.commands.import_csv import Command as ImportCommand
from csvapi.management.commands.barcodes_left import Command as BarcodesCommand
from csvapi.management.commands.top_customers import Command as CustomersCommand


@pytest.fixture(scope='function')
def import_command(request):
    return ImportCommand()


@pytest.fixture(scope='function')
def barcodes_command(request):
    return BarcodesCommand()


@pytest.fixture(scope='function')
def customers_command(request):
    return CustomersCommand()

