from django.contrib import admin
from import_export import admin as export_admin
from import_export import fields, resources, widgets

from csvapi.models import Customer, Order, OrderItem, Product


class CustomManyToManyWidget(widgets.ManyToManyWidget):
     def render(self, value, obj=None):
         rendered = super().render(value, obj=None)
         return '[%s]' % rendered


class OrderResource(resources.ModelResource):
        customer = fields.Field(
                column_name='customer_id',
                attribute='customer',
                widget=widgets.ForeignKeyWidget(Customer))

        order_items = fields.Field(
                column_name='barcodes',
                attribute='order_items',
                widget=CustomManyToManyWidget(Product))

        def get_queryset(self):
            queryset = super().get_queryset()
            return queryset.order_by('customer__customer_id')

        class Meta:
            model = Order
            fields = ('customer', 'order_id', 'order_items',)
            export_order = ('customer', 'order_id', 'order_items',)



class OrderItemInLine(admin.TabularInline):
    model = OrderItem
    extra = 1

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('customer_id',)
    model = Customer


@admin.register(Order)
class OrderAdmin(export_admin.ImportExportModelAdmin):
    resource_class = OrderResource
    list_display = ('order_id', 'customer',)
    inlines = (OrderItemInLine,)



@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    model = Product
    inlines = (OrderItemInLine,)
