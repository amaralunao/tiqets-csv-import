API to import and export CSV data
==============================

### The functionality of this project is to create a Django API that imports csv files that contain correspondence between orders and customers and orders and barcodes and create database records of orders, customers and products to be used to export information in csv format (or other formats as well) via the admin page

---

Steps to use:

__Import__:

* install in your virtualenv requirements.txt (pip install -r requirements.txt for unix based os)
* orders.csv needs to be imported before barcodes.csv due to db models foreignkey restrictions
* run from terminal myapy/manage.py migrate
* run from terminal myapi/manage.py import_csv orders '/path/to/orders.csv' 
* run from terminal myapi/manage.py import_csv barcodes '/path/to/barcodes.csv' 

__Export__:

* run from terminal myapi/manage.py createsuperuser => create admin user
* run from terminal myapi/manage.py runserver
* navigate to 127.0.0.1:8000/amin or localhost:8000/amin and login with previously created superuser
* the admin Site Administration will be available for navigation
* click on Orders link to navigate to a list display of all the orders and their corresponding customer_id
* if desired, click on the CUSTOMER tab to order by customer_id
* click on the EXPORT button from top-right of screen to be taken to the export view
* choose from drop-down the desired format(csv, xls, json etc.) and the click on SUBMIT 
* if the sorting by customer_id ascending is important, the export needs to be done with sorting: http://localhost:8000/admincsvapi/order/?o=2.1 
* for more details about the package used to achieve this: see django-import-export [link](http://django-import-export.readthedocs.io/en/stable/)

__Visualization__:

* for printing the amount of unused barcodes to the output: 
run from terminal myapi/manage.py barcodes_left 
* for printing a tuple value list of top 5 customers by amount of barcodes:
run from terminal myapi/manage.py top_customers 
(Note: you can change the top you want displayed( e.g. top 10 customers) by
changing the value of the top_cusomers_count in the myapi/csvapi/constants.py

__Tests__:

you can run the test suite of the project with the commands:
* tox (for both python2.7 and 3.4)
* tox -e py34 (for python3.4)
* tox -e py27 (for python2.7)


__Solution 2__:

* run with python the script orders_csv_import_script.py
* in the same folder as the script you need to have orders.csv and barcodes.csv
* a file called output.csv will be generated in same folder with the list of customer_id, order_id and barcodes
* a console output with top customers and unused barcodes will also be displayed
